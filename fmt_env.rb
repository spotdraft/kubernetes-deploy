x = YAML.load_file(".gitlab-ci.yml")[ENV["CI_BUILD_NAME"]]["variables"].sort

x.map do |k, v|
  puts("        - name: #{k}")
  puts("          value: \"#{v}\"")
end
